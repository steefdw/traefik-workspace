CREATE DATABASE IF NOT EXISTS `example_lamp`;

# create development user and grant rights
CREATE USER 'development'@'%' IDENTIFIED BY 'development';
GRANT ALL ON *.* TO 'development'@'%';
