#!/usr/bin/env bash

chmod 777 -R storage vendor bootstrap/cache/

composer install --no-interaction --prefer-source

# Create tables
php artisan migrate
php artisan db:seed

source /etc/apache2/envvars
exec apache2 -D FOREGROUND
