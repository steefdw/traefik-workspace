# LAMP stack example

## Getting started 

Go to the `{project-root}/examples/lamp-stack` directory and execute these commands:

##### 1 - Build docker
``` 
docker-compose build
```
  
##### 2 - Start the project

``` 
docker start traefik2
docker-compose up -d
```

##### 3 - Modify the `hosts` file on your machine by adding this line:
```
127.0.0.1  lamp.docker.test
```

##### 4 - See the result

Go to [https://lamp.docker.test](https://lamp.docker.test/) and you should see that everything is working:
![traefik-workspace-lamp-example](https://gitlab.com/steefdw/traefik-workspace/uploads/a4f87cf88183e0f6092a2f2cebbd479b/traefik-workspace-lamp-example.png)
