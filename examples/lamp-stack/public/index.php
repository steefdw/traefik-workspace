<html>
<head>
    <title>TW: LAMP stack example</title>
    <style>
        #example-info {
            margin: 1em auto;
            text-align: left;
            width: 934px;
        }
        #example-info h1 {
            background-color: #99c;
        }
        #example-info h2 {
            background-color: #ccf;
        }
        #example-info h1, #example-info h2 {
            padding: 5px;
            border: 1px solid #222;
            box-shadow: 1px 2px 3px #ccc;
        }
        .success {
            color: #070;
        }
        .potential-warning,.warning {
            color: #900;
        }
        .potential-warning, .potential-warning br {
            display: none;
        }
    </style>
</head>
<body>
<div id="example-info">
<h1>Traefik Workspace - LAMP stack example</h1>

<h2>Database info</h2>
<ul>
<?php
$hostname = 'db-lamp';
$username = 'root';
$password = 'root';
$database = 'example_lamp';

echo '<li class="potential-warning warning">';
$link = new mysqli($hostname, $username, $password, $database);
echo '</li>';

if (mysqli_connect_error()) {
    echo '<li class="warning"><strong>Error:</strong> Could not make a database link (' . $link->connect_errno . '): ' . $link->connect_error . '</li>';
    echo '<li class="warning"><strong>Error:</strong> Could not connect to <strong>' . $hostname . '</strong></li>';
}
else {
    echo '<li class="success">Connected to host <strong>' . $link->host_info . '</strong> running MySQL ' . $link->get_server_info() . '</li>';
    $link->close();
}
?>
</ul>


<h2>PHP info</h2>
</div>
<?php echo phpinfo();