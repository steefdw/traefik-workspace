# Hello World example

## Getting started 

Go to the `{project-root}/examples/hello-world` directory and execute these commands:

##### 1 - Start the project

``` 
docker start traefik2
docker-compose up -d
```

You will see the example service popping up on the "http" page in the [traefik.docker.test](https://traefik.docker.test/) dashboard

![traefik-workspace-dashboard-routers](https://gitlab.com/steefdw/traefik-workspace/uploads/0a773d2500ea1466d1cd31d8f2ade206/traefik-workspace-dashboard-routers.png)

##### 2 - Modify the `hosts` file on your machine by adding this line:
```
127.0.0.1  example.docker.test
```

##### 3 - See the result

Go to [https://example.docker.test](https://example.docker.test/) and you should see that everything is working