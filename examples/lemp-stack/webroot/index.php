<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>LEMP example</title>
    <style>
        li {
            display: inline-block;
            padding: 0.2em 1em;
            border: 1px solid #ccc;
            margin-left: -1px;
        }

        li a, li a:link {
            color: #333;
        }

        ul {
            margin: .4em 0 1em;
            padding: 0;
            display: inline-block;
        }
    </style>
</head>
<body>

<div>
    <strong>used images:</strong>
    <ul>
        <li>
            <a href="https://hub.docker.com/_/alpine/" title="Alpine Linux">Alpine Linux</a>
        <li>
            <a href="https://hub.docker.com/_/nginx/" title="Nginx">Nginx</a>
        <li>
            <a href="https://hub.docker.com/_/mariadb/" title="MariaDB">MariaDB</a>
        <li>
            <a href="https://hub.docker.com/_/php/" title="PHP">PHP</a>
        <li>
            <a href="https://hub.docker.com/r/phpmyadmin/phpmyadmin/" title="phpMyAdmin">phpMyAdmin</a>
    </ul>
</div>

<div>
    <strong>services:</strong>
    <ul>
        <li>
            <a href="https://phpmyadmin.docker.test/">phpmyadmin.docker.test</a>
        </li>
    </ul>
</div>

<?= phpinfo() ?>

</body>
</html>

