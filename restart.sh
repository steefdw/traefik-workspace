#!/usr/bin/env bash

##################################################################################################
# Sometimes the Traefik dashboard will not load after running `docker start traefik2`
# To fix this, simply run this script, and https://traefik.docker.test/ will be working again.
##################################################################################################

create_container() {
    docker run -d \
      -v /var/run/docker.sock:/var/run/docker.sock \
      -v $PWD/traefik.yml:/etc/traefik/traefik.yml \
      -v $PWD/certs:/certs \
      -p 8080:8080 \
      -p 80:80 \
      -p 443:443 \
      -l "traefik.enable=true" \
      -l "traefik.http.services.api.loadbalancer.server.port=443" \
      -l "traefik.http.routers.api.entrypoints=https" \
      -l 'traefik.http.routers.api.rule=Host(`traefik.docker.test`)' \
      -l "traefik.http.routers.api.service=api@internal" \
      -l "traefik.http.routers.api.tls=true" \
      -l 'traefik.http.routers.traefik-insecure.rule=Host(`traefik.docker.test`)' \
      -l "traefik.http.routers.traefik-insecure.entrypoints=http" \
      -l "traefik.http.routers.traefik-insecure.middlewares=redirect@file" \
      -l 'traefik.http.routers.traefik.rule=Host(`traefik.docker.test`)' \
      -l "traefik.http.routers.traefik.entrypoints=https" \
      -l "traefik.http.routers.traefik.tls=true" \
      --network traefik-network \
      --name traefik2 \
      traefik:v2.1.2
    echo "Container 'traefik2' created"
}

create_network() {
    docker network create traefik-network
    echo "Network 'traefik-network' created"
}

# remove the "traefik2" container + "traefik-network" network
remove_all() {
    docker stop traefik2
    docker container rm traefik2
    echo "Container 'traefik2' removed"

    docker network rm traefik-network
    echo "Network 'traefik-network' removed"
}

remove_all
create_network
create_container

echo 'starting traefik v2.1...'
docker start traefik2