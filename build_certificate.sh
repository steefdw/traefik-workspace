#!/usr/bin/env bash

echo "[34m
  ██████╗███████╗██████╗ ████████╗██╗███████╗██╗ ██████╗ █████╗ ████████╗███████╗
 ██╔════╝██╔════╝██╔══██╗╚══██╔══╝██║██╔════╝██║██╔════╝██╔══██╗╚══██╔══╝██╔════╝
 ██║     █████╗  ██████╔╝   ██║   ██║█████╗  ██║██║     ███████║   ██║   █████╗
 ██║     ██╔══╝  ██╔══██╗   ██║   ██║██╔══╝  ██║██║     ██╔══██║   ██║   ██╔══╝
 ╚██████╗███████╗██║  ██║   ██║   ██║██║     ██║╚██████╗██║  ██║   ██║   ███████╗
  ╚═════╝╚══════╝╚═╝  ╚═╝   ╚═╝   ╚═╝╚═╝     ╚═╝ ╚═════╝╚═╝  ╚═╝   ╚═╝   ╚══════╝

      ██████╗ ███████╗███╗   ██╗███████╗██████╗  █████╗ ████████╗ ██████╗ ██████╗
     ██╔════╝ ██╔════╝████╗  ██║██╔════╝██╔══██╗██╔══██╗╚══██╔══╝██╔═══██╗██╔══██╗
     ██║  ███╗█████╗  ██╔██╗ ██║█████╗  ██████╔╝███████║   ██║   ██║   ██║██████╔╝
     ██║   ██║██╔══╝  ██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║   ██║   ██║   ██║██╔══██╗
     ╚██████╔╝███████╗██║ ╚████║███████╗██║  ██║██║  ██║   ██║   ╚██████╔╝██║  ██║
      ╚═════╝ ╚══════╝╚═╝  ╚═══╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝
[0m
[33m Create the self-signed certificate for your workspace on *.docker.test[0m
================================================================================="
# Directories
cur=`pwd`
tmp=`mktemp -d`
scriptName=`basename $0`

# Certificate Variables
OUTPATH=${cur}"/certs/"
CN="*.docker.test"
DURATION=3650 # 10 years

# Help Screen
help() {
  echo -n "Usage: ./${scriptName} [OPTIONS]

Generate self-signed TLS certificate using OpenSSL

 Options:
  -c|--country         Country Name (2 letter code)
  -s|--state           State or Province Name (full name)
  -l|--locality        Locality Name (eg, city)
  -o|--organization    Organization Name (eg, company)
  -u|--unit            Organizational Unit Name (eg, section)
  -n|--common-name     Common Name (e.g. server FQDN or YOUR name)
  -p|--path            Path to output generated keys
  -d|--duration        Validity duration of the certificate (in days)
  -h|--help            Display this help and exit
"
}

# Prompt for variables that were not provided in arguments
checkVariables() {
  # Country
  if [[ -z "$C" ]]; then
    echo -n "Country Name (2 letter code) [NL]:"
    read -r C
  fi

  # State
  if [[ -z "$ST" ]]; then
    echo -n "State or Province Name (full name) [Noord-Holland]:"
    read -r ST
  fi

  # Locality
  if [[ -z "$L" ]]; then
    echo -n "Locality Name (eg, city) [Amsterdam]:"
    read -r L
  fi

  # Organization
  if [[ -z "$O" ]]; then
    echo -n "Organization Name (eg, company) [MyCompany Ltd]:"
    read -r O
  fi

  # Organizational Unit
  if [[ -z "$OU" ]]; then
    echo -n "Organizational Unit Name (eg, section) [MyCompany Development]:"
    read -r OU
  fi

  # Common Name
  if [[ -z "$CN" ]]; then
    echo -n "Common Name (e.g. server FQDN or YOUR name) [leave empty for '*.docker.test']:"
    read -r CN
    if [[ -z "${CN// }" ]]; then
        CN="*.docker.test"
    fi
  fi
}

safeExit() {
  if [[ -d $tmp ]]; then
    rm -rf $tmp
  fi

  trap - INT TERM EXIT
  exit
}

# Show variable values
showValues() {
  echo "Country: ${C}";
  echo "State: ${ST}";
  echo "Locality: ${L}";
  echo "Organization: ${O}";
  echo "Organization Unit: ${OU}";
  echo "Common Name: ${CN}";
  echo "Output Path: ${OUTPATH}";
  echo "Certificate Duration (Days): ${DURATION}";
}

buildCsrCnf() {
cat << EOF > "${tmp}/tmp.csr.cnf"
[req]
default_bits = 2048
prompt = no
default_md = sha256
distinguished_name = dn
[dn]
C=${C}
ST=${ST}
L=${L}
O=${O}
OU=${OU}
CN=${CN}
EOF
}

buildExtCnf() {
cat << EOF > "${tmp}/v3.ext"
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = ${CN}
EOF
}

# Build TLS Certificate
build() {
  # Santizie domain name for file name
  FILENAME=${CN/\*\./}
  # Generate CA key & crt
  openssl genrsa -out ${tmp}/tmp.key 2048
  openssl req -x509 -new -nodes -key ${tmp}/tmp.key -sha256 -days ${DURATION} -out ${OUTPATH}${FILENAME}_CA.pem -subj "/C=${C}/ST=${ST}/L=${L}/O=${O}/OU=${OU}/CN=${CN}"

  # CSR Configuration
  buildCsrCnf

  # Create v3.ext configuration file
  buildExtCnf

  # Server key
  openssl req -new -sha256 -nodes -out ${OUTPATH}${FILENAME}.csr -newkey rsa:2048 -keyout ${OUTPATH}${FILENAME}.key -config <( cat ${tmp}/tmp.csr.cnf )

  # Server certificate
  openssl x509 -req -in ${OUTPATH}${FILENAME}.csr -CA ${OUTPATH}${FILENAME}_CA.pem -CAkey ${tmp}/tmp.key -CAcreateserial -out ${OUTPATH}${FILENAME}.crt -days ${DURATION} -sha256 -extfile ${tmp}/v3.ext
}

doneMessage() {
    echo "[32mDone!!![0m

Add the ${OUTPATH}${FILENAME}_CA.pem file in Chrome:

1) Settings > Advanced > Manage certificates
2) Click "import" in the "authorities" tab
3) Visit https://traefik.docker.test
"
}

# Process Arguments
while [[ "$1" != "" ]]; do
  PARAM=$(echo "$1" | awk -F= '{print $1}')
  VALUE=$(echo "$1" | awk -F= '{print $2}')
  case $PARAM in
    -h|--help) help; safeExit ;;
    -c|--country) C=$VALUE ;;
    -s|--state) ST=$VALUE ;;
    -l|--locality) L=$VALUE ;;
    -o|--organization) O=$VALUE ;;
    -u|--unit) OU=$VALUE ;;
    -n|--common-name) CN=$VALUE ;;
	-d|--duration) DURATION=$VALUE ;;
    *) echo "ERROR: unknown parameter \"$PARAM\""; help; exit 1 ;;
  esac
  shift
done

checkVariables
build
showValues
doneMessage
safeExit