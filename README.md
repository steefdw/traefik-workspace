# Traefik Workspace

Run multiple docker-compose projects in your workspace at the same time with Traefik.

#### Use case
So you have a workspace like this:
```
~/workspace/my-project/
~/workspace/other-project
~/workspace/another-project
```

And you want to have them available on:
```
https://my-project.docker.test/
https://other-project.docker.test/
https://another-project.docker.test/
```

Without touching the apache/nginx configs? And DO want them available with TLS?

Simply add some labels to the docker-compose.yml file of your project to have it available on https://**projectname**.docker.test in about a minute!

#### Initial setup
Note: you only have to do this initial setup once.

(1) Build a self-signed certificate with: 
```bash
bash build_certificate.sh
```

(2) Run `init.sh`. This will pull the Traefik image and runs the container with the correct settings.

If `init.sh` is not executable on you system, just do:
```bash    
sh init.sh
```

(3) Add this to your host file
```bash
127.0.0.1 traefik.docker.test
``` 
and go to [traefik.docker.test](https://traefik.docker.test/). 
You should see the Traefik dashboard.

#### Re-starting the network
After a reboot (or manually stopping the "traefik-network"), you have to restart the network.
No more need for running `init.sh`, just simply:

(1) Run in your terminal:
```
docker start traefik2
```

(2) Go to [traefik.docker.test](https://traefik.docker.test/). You should see the Traefik dashboard

![traefik-workspace-dashboard](https://gitlab.com/steefdw/traefik-workspace/uploads/97b95869b28085211ecfdb6d8c77dfb5/traefik-workspace-dashboard.png)

## Adding services

#### Getting started
See the Traefik docs on how to [deploy new services](https://docs.traefik.io/v2.1/getting-started/quick-start/#traefik-detects-new-services-and-creates-the-route-for-you)

You can also check out these examples:
* [Hello World for Traefik-workspace](https://gitlab.com/steefdw/traefik-workspace/-/tree/master/examples/hello-world), a simple example to check that everything is working.
* [Traefik-workspace with a LAMP stack](https://gitlab.com/steefdw/traefik-workspace/-/tree/master/examples/lamp-stack) (web server with Linux, Apache, MySQL and PHP)

(2) Run:
```bash
docker-compose up -d
```
(3) You will see the example service popping up on the "http" page in the [traefik.docker.test](https://traefik.docker.test/) dashboard

![traefik-workspace-dashboard-routers](https://gitlab.com/steefdw/traefik-workspace/uploads/0a773d2500ea1466d1cd31d8f2ade206/traefik-workspace-dashboard-routers.png)

(4) Add `127.0.0.1 example.docker.test` in your hosts file

(5) Go to [https://example.docker.test](https://example.docker.test) and see that it is working

#### Allow my projects to run on Traefik
For every service you want to be reachable under **projectname**.docker.test, just add these labels:
```yaml
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.whoami-insecure.rule=Host(`example.docker.test`)"
      - "traefik.http.routers.whoami-insecure.entrypoints=http"
      - "traefik.http.routers.whoami-insecure.middlewares=redirect@file"
      - "traefik.http.routers.whoami.rule=Host(`example.docker.test`)"
      - "traefik.http.routers.whoami.entrypoints=https"
      - "traefik.http.routers.whoami.tls=true"
    networks:
      - traefik-network
```

The `networks` section of the docker-compose.yml should have the `proxy` network defined like this:

```yaml
networks:
  traefik-network:
    external: true
```

Also see the `docker-compose.yml` files from the examples.